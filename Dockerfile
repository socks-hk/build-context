FROM docker:latest

RUN apk add --update \
    python \
    python-dev \
    py-pip \
    build-base \
  && pip install virtualenv awscli \
  && rm -rf /var/cache/apk/*